//
//  MKMapView+Zoom.m
//  EventsHub
//
//  Created by alexander on 5/30/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "MKMapView+Zoom.h"

@implementation MKMapView (Zoom)
- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel animated:(BOOL)animated {
    MKCoordinateSpan span = MKCoordinateSpanMake(0, 360/pow(2, zoomLevel)*self.frame.size.width/256);
    [self setRegion:MKCoordinateRegionMake(centerCoordinate, span) animated:animated];
}
-(double) getZoomLevel {
    return log2(360 * ((self.frame.size.width/256) / self.region.span.longitudeDelta));
}

@end
