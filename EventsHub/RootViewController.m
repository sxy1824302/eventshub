//
//  RootViewController.m
//  EventsHub
//
//  Created by alexander on 8/19/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "RootViewController.h"
#import "FirstLoginViewController.h"
@interface RootViewController ()

@end

@implementation RootViewController


-(void)awakeFromNib{
    self.backgroundImage = [UIImage imageNamed:@"menu_bg"];
    

    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BrowseEventsTableViewControllerNavi"];
    
    
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewContriller"];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
