//
//  BaseObject.h
//  EventsHub
//
//  Created by alexander on 7/28/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//
#import <JSONModel.h>

@interface BaseObject : JSONModel

@property (nonatomic)NSString *create_date;
@property (nonatomic)NSString *update_date;
@property (nonatomic)NSString *_id;


@end
