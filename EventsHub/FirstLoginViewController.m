//
//  FirstLoginViewController.m
//  EventsHub
//
//  Created by alexander on 9/1/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "FirstLoginViewController.h"
#import "FillProfileViewController.h"
#import "LoginController.h"
#import <RESideMenu.h>
@interface FirstLoginViewController ()

@end

@implementation FirstLoginViewController


- (IBAction)completeInfo:(id)sender {
    
    FillProfileViewController *pvc =[[FillProfileViewController alloc] init];
    
    pvc.firstController = self;
    
    UINavigationController *nv =[[UINavigationController alloc] initWithRootViewController:pvc];

    [self presentViewController:nv animated:YES completion:^{
        
    }];
    
}

-(void)gotoBrowseView{

    self.sideMenuViewController.contentViewController= [self.storyboard instantiateViewControllerWithIdentifier:@"BrowseEventsTableViewControllerNavi"];
    
}

- (IBAction)login:(id)sender {
    
    LoginController *pvc =[[LoginController alloc] init];
    
    pvc.firstController = self;
    
    UINavigationController *nv =[[UINavigationController alloc] initWithRootViewController:pvc];
    
    [self presentViewController:nv animated:YES completion:^{
        
    }];
    
}

- (IBAction)continueUse:(id)sender {
    
    self.sideMenuViewController.contentViewController= [self.storyboard instantiateViewControllerWithIdentifier:@"BrowseEventsTableViewControllerNavi"];
    
}

@end
