//
//  Event.h
//  EventsHub
//
//  Created by alexander on 7/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "BaseObject.h"
#import "Comment.h"
@interface Event :BaseObject

@property (nonatomic)NSString *created_by;

@property (nonatomic)NSString *name;

@property (nonatomic)NSString *event_dsc;

@property (nonatomic)NSNumber *longitude;

@property (nonatomic)NSNumber *latitude;

@property (nonatomic)NSString *address_dsc;

@property (nonatomic)NSString *phone_number;

@property (nonatomic)NSString *lang;

@property (nonatomic)NSString *start_date;

@property (nonatomic)NSString *city;

@property (nonatomic)NSString *state;

//


@property(nonatomic)NSArray *comments;
@property (nonatomic)NSArray *participants;
@property (nonatomic)NSArray *likedPeople;
@property (nonatomic)NSDate *startDate;

-(NSString*)joinURL;
-(NSString*)shareURL;



@end
