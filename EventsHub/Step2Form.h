//
//  Step2Form.h
//  EventsHub
//
//  Created by alexander on 8/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FXForms.h>
@interface Step2Form : NSObject<FXForm>
@property (nonatomic)NSDate *startDate;
@end
