//
//  CommentsTableViewController.h
//  EventsHub
//
//  Created by alexander on 8/12/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Event;
@interface CommentsTableViewController : UIViewController
@property (nonatomic,weak)Event *event;
@end
