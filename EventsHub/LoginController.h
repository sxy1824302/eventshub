//
//  LoginController.h
//  EventsHub
//
//  Created by alexander on 10/11/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "FXForms.h"
@class FirstLoginViewController;

@interface LoginController : FXFormViewController
@property (nonatomic,weak)FirstLoginViewController *firstController;
@end
