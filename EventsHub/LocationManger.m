//
//  LocationManger.m
//  EventsHub
//
//  Created by alexander on 5/30/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "LocationManger.h"
#import "DPAPI.h"
@interface LocationManger ()<DPRequestDelegate,CLLocationManagerDelegate>{
    DPAPI *_api;
}

@end

@implementation LocationManger{
    CLLocationManager*_locationManger;

    ReverseGeocoding _completeBlock;
    
}
+ (id)sharedLocationManger
{
    static dispatch_once_t onceQueue;
    static LocationManger *locationManger = nil;
    
    dispatch_once(&onceQueue, ^{ locationManger = [[self alloc] init]; });
    return locationManger;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _locationManger = [[CLLocationManager alloc] init];
        
        _locationManger.delegate = self;
        
        _locationManger.distanceFilter = kCLDistanceFilterNone;
        
        _locationManger.desiredAccuracy = kCLLocationAccuracyBest;
        
        [_locationManger startUpdatingLocation];
        
        _api = [[DPAPI alloc] init];

    }
    return self;
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation* currentLocation = [locations firstObject];
    
    [_locationManger stopUpdatingLocation];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placeMark = [placemarks firstObject];
        
        if (!placemarks) {
            
            DDLogError(@"didnt have a place mark");
            
        }else{
            
            _userPlaceMark = placeMark;
            
            DDLogVerbose(@"current place %@",placeMark);
        }
 
    }];

}


- (void)searchReGeocodeWithLocation:(CLLocationCoordinate2D)location complete:(ReverseGeocoding)complete
{
    _completeBlock = complete;
    
    [_api requestWithURL:@"v1/business/find_businesses" params:[@{@"latitude":@(location.latitude),@"longitude":@(location.longitude),@"sort":@7} mutableCopy] delegate:self];
}
- (void)request:(DPRequest *)request didFailWithError:(NSError *)error {
   
}

- (void)request:(DPRequest *)request didFinishLoadingWithResult:(id)result {
    NSArray *bussiness = result[@"businesses"];
    
    DDLogVerbose(@"%@",bussiness);
    
    if (_completeBlock) {
        _completeBlock(bussiness);
    }
}
@end
