//
//  EventsTableViewController.h
//  EventsHub
//
//  Created by alexander on 6/9/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewController.h"
@interface EventsTableViewController : CustomTableViewController

@end
