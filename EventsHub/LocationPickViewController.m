//
//  LocationPickViewController.m
//  EventsHub
//
//  Created by alexander on 5/30/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "LocationPickViewController.h"
#import <MapKit/MapKit.h>
#import "MKMapView+Zoom.h"
#import "LocationManger.h"
#import "POITableViewCell.h"
#import <AddressBookUI/AddressBookUI.h>
@interface LocationPickViewController ()<MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate>{
    NSArray *_locations;
    
    CGFloat _zoomLevel;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@end

@implementation LocationPickViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _locations =@[];
    
    _zoomLevel = 14;

    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.tabBarController.tabBar setHidden:NO];
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
   
}


- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error{
    
    DDLogVerbose(@"ERROR:  %@",error.localizedDescription);
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    
    [mapView setCenterCoordinate:userLocation.coordinate zoomLevel:_zoomLevel animated:YES];
    
    [self geoCodingCurrentLocation];

}

- (void)geoCodingCurrentLocation{
    
    @weakify(self);
    [[LocationManger sharedLocationManger]searchReGeocodeWithLocation:_mapView.userLocation.location.coordinate complete:^(NSArray *locations) {
        @strongify(self);
        
        self->_locations = locations;

        [self.tableView reloadData];
        
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _locations.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    POITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"POITableViewCell" forIndexPath:indexPath];
    
    NSDictionary *dic = _locations[indexPath.row];
    
    cell.nameLabel.text = dic[@"name"];
    cell.addressLabel.text = dic[@"address"];

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
