//
//  AccessRecord.h
//  EventsHub
//
//  Created by alexander on 9/1/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "JSONModel.h"
#import "BaseObject.h"
@interface AccessRecord : BaseObject
@property (nonatomic)NSString *access_token;
@end
