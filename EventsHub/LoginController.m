//
//  LoginController.m
//  EventsHub
//
//  Created by alexander on 10/11/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "LoginController.h"
#import "LoginForm.h"
#import "AccountControl.h"
#import <MBProgressHUD.h>
#import "FirstLoginViewController.h"
@implementation LoginController
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.formController.form = [[LoginForm alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    
    self.navigationItem.leftBarButtonItem = cancel;
    
}
-(void)cancel:(UIBarButtonItem*)btn{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}

- (void)submitLoginForm
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    LoginForm*form = (LoginForm*)self.formController.form;
    
    [[AccountControl sharedAccountControl] loginWithUserName:form.email password:form.password succeed:^(BOOL succeed) {
       
        if (succeed) {
            [self.presentingViewController dismissViewControllerAnimated:NO completion:^{
                
                [self.firstController gotoBrowseView];
                
            }];
        }
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

@end
