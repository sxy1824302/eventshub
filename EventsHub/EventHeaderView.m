//
//  EventHeaderView.m
//  EventsHub
//
//  Created by 孙翔宇 on 8/10/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "EventHeaderView.h"

@implementation EventHeaderView

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        UILabel* dateLabel = [[UILabel alloc] initWithFrame:self.bounds];
        
        dateLabel.text = @"Not Now";
        
        dateLabel.font = [UIFont systemFontOfSize:14];
        
        dateLabel.textAlignment = NSTextAlignmentRight;
        
        [self.contentView addSubview:dateLabel];
        
        _dateLabel = dateLabel;

        self.contentView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
        
        self.dateLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[dateLabel(>=40)]-4-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(dateLabel)]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-2-[dateLabel(>=14)]-2-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(dateLabel)]];
    }
    
    return self;
}

@end
