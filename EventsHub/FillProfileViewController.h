//
//  FillProfileViewController.h
//  EventsHub
//
//  Created by alexander on 9/1/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "FXForms.h"
@class FirstLoginViewController;

@interface FillProfileViewController : FXFormViewController
@property (nonatomic,weak)FirstLoginViewController *firstController;
@end
