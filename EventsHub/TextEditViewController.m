//
//  TextEditViewController.m
//  EventsHub
//
//  Created by alexander on 8/21/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "TextEditViewController.h"
#import "AccountControl.h"
#import "AccountControl.h"
@interface TextEditViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textField;
@end

@implementation TextEditViewController


- (IBAction)cancel:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (IBAction)save:(id)sender {
    
    Account*acc = [[AccountControl sharedAccountControl] account];
    
    [[AccountControl sharedAccountControl] updateAccount:^(BOOL succeed) {
        
        if (succeed) {
            [acc setValue:_textField.text forKey:_updateKey];
            
            [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
                
            }];
        }
    } withInfo:@{_updateKey:self.textField.text}];
    

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
