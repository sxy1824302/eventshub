//
//  Step1Form.m
//  EventsHub
//
//  Created by alexander on 8/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "Step1Form.h"

@implementation Step1Form
-(NSArray*)fields{
    return @[
             @"name",
             @{FXFormFieldKey: @"phone", FXFormFieldType: FXFormFieldTypeText},
             @{FXFormFieldKey: @"basicDescription", FXFormFieldType: FXFormFieldTypeLongText, FXFormFieldPlaceholder: @"Description"},
             ];
}
@end
