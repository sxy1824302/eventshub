//
//  GeneralParticipantTableViewCell.h
//  EventsHub
//
//  Created by alexander on 8/5/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GeneralParticipantTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contactLabel;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end
