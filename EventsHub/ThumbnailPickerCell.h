//
//  ThumbnailPickerCell.h
//  EventsHub
//
//  Created by alexander on 9/5/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "FXForms.h"

@interface ThumbnailPickerCell : FXFormBaseCell

@property (nonatomic,readonly) UIImageView *imagePickerView;

@property (nonatomic,readonly) UIImagePickerController *imagePickerController;

@end
