//
//  Participant.m
//  EventsHub
//
//  Created by alexander on 7/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "Participant.h"
#import "NSDate+UTC.h"
@implementation Participant



+(BOOL)propertyIsOptional:(NSString *)propertyName{
    if ([propertyName isEqualToString:@"attend_date"]||[propertyName isEqualToString:@"account_ref"]) {
        return YES;
    }
    return NO;
}

+(BOOL)propertyIsIgnored:(NSString *)propertyName{
    if ([propertyName isEqualToString:@"attendDate"]) {
        return YES;
    }
    return NO;
}
-(void)setAttend_date:(NSString *)attend_date{
    _attend_date = attend_date;
    _attendDate = [NSDate dateFromUTCString:attend_date];
}

@end
