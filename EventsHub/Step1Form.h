//
//  Step1Form.h
//  EventsHub
//
//  Created by alexander on 8/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FXForms.h>
@interface Step1Form : NSObject<FXForm>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *basicDescription;

@end
