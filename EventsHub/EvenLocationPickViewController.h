//
//  EvenLocationPickViewController.h
//  EventsHub
//
//  Created by 孙翔宇 on 7/6/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
@class  Event;

@interface EvenLocationPickViewController : UIViewController
@property (nonatomic)Event *event;
@end
