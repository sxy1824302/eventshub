//
//  ECStep2ViewController.h
//  EventsHub
//
//  Created by alexander on 7/9/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <FXForms.h>
@class Event;

@interface ECStep2ViewController : FXFormViewController

@property (nonatomic)Event *even;

@end
