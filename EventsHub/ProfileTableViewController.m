//
//  ProfileTableViewController.m
//  EventsHub
//
//  Created by alexander on 8/21/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "ProfileTableViewController.h"
#import "AccountControl.h"
#import <RESideMenu.h>
#import "TextEditViewController.h"

#define kCellName @"cellName"
#define kKeyName @"keyName"

@interface ProfileTableViewController (){
    NSArray *_profileSettings;
}

@end

@implementation ProfileTableViewController
- (IBAction)showMenu:(id)sender {
     [self.sideMenuViewController presentLeftMenuViewController];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _profileSettings = @[@{kCellName:@"Name",kKeyName:@"name"},
                         @{kCellName:@"Phone",kKeyName:@"phone_number"},
                         @{kCellName:@"Email",kKeyName:@"email"}];
    
    
    
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return _profileSettings.count;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"infocell" forIndexPath:indexPath];
    
    cell.textLabel.text = _profileSettings[indexPath.row][kCellName];
    
    Account *account = [[AccountControl sharedAccountControl] account];
    NSString *value ;
    switch (indexPath.row) {
        case 0:
            value =account.name;
            break;
        case 1:
            value =account.phone;
            break;
        case 2:
            value =account.email;
            break;
            
        default:
            break;
    }
    
    if (!value) {
        
        value = @"Tap to set";
    }
    
    cell.detailTextLabel.text = value;
    
    
    return cell;
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    TextEditViewController *des = [segue destinationViewController];
    des.updateKey =_profileSettings[indexPath.row][kKeyName];
    
}


@end
