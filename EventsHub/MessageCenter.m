//
//  MessageCenter.m
//  EventsHub
//
//  Created by 孙翔宇 on 8/2/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "MessageCenter.h"
#import <TSMessage.h>
#import "AppDelegate.h"
@implementation MessageCenter
+(void)showSuccess:(NSString*)successMessege inViewController:(UIViewController*)vc{
    
}

+(UIViewController*)root{
    return [[[UIApplication sharedApplication] keyWindow] rootViewController];
}

+(UIViewController*)controller{
    

    UIViewController*topVc;
    
    if ([[MessageCenter root] isKindOfClass:[UINavigationController class]]) {
        topVc =  [(UINavigationController*)[MessageCenter root] visibleViewController];
    }else{
        topVc = [MessageCenter root];
    }
    
    if (topVc.presentedViewController) {
        
        topVc = topVc.presentedViewController;
    }
    
    return topVc;
}
+(void)showFailed:(NSString*)failedMessage inViewController:(UIViewController*)vc{
    
    [TSMessage showNotificationInViewController:[MessageCenter controller] title:nil subtitle:failedMessage type:TSMessageNotificationTypeError];
    DDLogError(failedMessage);
    
}
@end
