//
//  MessageCenter.h
//  EventsHub
//
//  Created by 孙翔宇 on 8/2/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageCenter : NSObject


+(void)showSuccess:(NSString*)successMessege inViewController:(UIViewController*)vc;

+(void)showFailed:(NSString*)failedMessage inViewController:(UIViewController*)vc;

@end
