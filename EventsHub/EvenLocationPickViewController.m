//
//  EvenLocationPickViewController.m
//  EventsHub
//
//  Created by 孙翔宇 on 7/6/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "EvenLocationPickViewController.h"
#import <MapKit/MapKit.h>
#import "LocationManger.h"
#import "POITableViewCell.h"
#import "MKMapView+Zoom.h"
#import "ECFinalViewController.h"
#import "EventManager.h"
#import <MBProgressHUD.h>

@interface EvenLocationPickViewController ()<MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate>{
    NSArray *_locations;
    
    CGFloat _zoomLevel;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation EvenLocationPickViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _locations =@[];
    
    _zoomLevel = 14;
    
    
    @weakify(self);
    [EventManager createEvent:_event complete:^(NSDictionary *info){
        @strongify(self);
        self.event._id = info[@"_id"];
    }];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}
- (IBAction)gotoNext:(id)sender {
    
    
    ECFinalViewController *ecs1 = [[ECFinalViewController alloc]  initWithEvent:_event];

    [self.navigationController pushViewController:ecs1 animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.tabBarController.tabBar setHidden:NO];
}



- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error{
    
    DDLogVerbose(@"ERROR:  %@",error.localizedDescription);
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    
    [mapView setCenterCoordinate:userLocation.coordinate zoomLevel:_zoomLevel animated:YES];
    
    [self geoCodingCurrentLocation];
    
    _event.latitude =@( userLocation.location.coordinate.latitude);
    
    _event.longitude = @( userLocation.location.coordinate.longitude);
    
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    
    [geocoder reverseGeocodeLocation:userLocation.location completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placeMark = [placemarks firstObject];
        
        if (!placemarks) {
            
            DDLogError(@"didnt have a place mark");
            
        }else{
            
            _event.city = placeMark.locality;
            
            _event.state = placeMark.administrativeArea;
            
        }
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
}

- (void)geoCodingCurrentLocation{
    
    @weakify(self);
    [[LocationManger sharedLocationManger]searchReGeocodeWithLocation:_mapView.userLocation.location.coordinate complete:^(NSArray *locations) {
        @strongify(self);
        
        self->_locations = locations;
        
        [self.tableView reloadData];
        
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _locations.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    POITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"POITableViewCell" forIndexPath:indexPath];
    
    NSDictionary *dic = _locations[indexPath.row];
    
    cell.nameLabel.text = dic[@"name"];
    
    cell.addressLabel.text = dic[@"address"];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = _locations[indexPath.row];
    
    _event.address_dsc =dic[@"name"];

    ECFinalViewController *ecs1 = [[ECFinalViewController alloc] initWithEvent:_event];

    [self.navigationController pushViewController:ecs1 animated:YES];
}

@end
