//
//  EventDetailTableViewController.m
//  EventsHub
//
//  Created by alexander on 7/11/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "EventParticipantsTableViewController.h"
#import "ParticipantTableViewCell.h"
#import "EventManager.h"
#import <TTTTimeIntervalFormatter.h>

#import "NetworkManager.h"
#import <SVWebViewController.h>
@interface EventParticipantsTableViewController (){
   

    TTTTimeIntervalFormatter *_timeFormatter;
}
@property (nonatomic) NSArray *participants;
@end

@implementation EventParticipantsTableViewController

-(void)setEvent:(Event *)event{
    _event = event;

    [self reloadParticipants];
    
    _timeFormatter =[[TTTTimeIntervalFormatter alloc] init];
    
}

-(void)reloadParticipants{
    @weakify(self);
    [EventManager loadEventParticipants:_event._id completed:^(NSArray *participants) {
        @strongify(self);
       
        self.participants = participants;
        
        
        [self.tableView reloadData];
    }];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    
}
- (IBAction)addMore:(id)sender {
    
    NSURL *url =[NSURL URLWithString: [NSString stringWithFormat:[HostAPI stringByAppendingPathComponent:@"JoinEvent/%@"],_event._id]];
    
   SVModalWebViewController*vc = [[SVModalWebViewController alloc] initWithURL:url];
    
    vc.modalPresentationStyle = UIModalPresentationPageSheet;
    [self presentViewController:vc animated:YES completion:NULL];

}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self reloadParticipants];
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _participants.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Participant *part =  _participants[indexPath.row];
    
    ParticipantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ParticipantTableViewCell" forIndexPath:indexPath];
    cell.defaultColor = tableView.backgroundColor;

    cell.nameLabel.text =part.name;

    UIView *clockView = [MCSwipeTableViewCell viewWithImageName:@"check_in"];

    UIColor *yellowColor = [UIColor colorWithRed:254.0 / 255.0 green:217.0 / 255.0 blue:56.0 / 255.0 alpha:1.0];
    
    if (part.attendDate) {
        
        cell.statusLabel.text = [NSString stringWithFormat:@"Registed %@",[_timeFormatter stringForTimeInterval:[part.attendDate timeIntervalSinceNow]]];
        
    }else{
        
        [cell setSwipeGestureWithView:clockView color:yellowColor mode:MCSwipeTableViewCellModeSwitch state:MCSwipeTableViewCellState3 completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            
            Participant *part =  _participants[indexPath.row];
            
            part.attendDate = [NSDate date];
            
            [EventManager participantAttend:part._id completed:^{
                
            }];
            
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
         cell.statusLabel.text = @"Not here yet ";
        
        
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 65;
}




-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    

}
@end
