//
//  ECFinalViewController.h
//  EventsHub
//
//  Created by alexander on 7/9/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <FXForms.h>
@class Event;

@interface ECFinalViewController : FXFormViewController
@property(nonatomic) Event *event;


- (instancetype)initWithEvent:(Event*)event;
@end
