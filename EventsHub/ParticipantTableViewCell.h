//
//  ParticipantTableViewCell.h
//  EventsHub
//
//  Created by alexander on 7/11/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MCSwipeTableViewCell.h>
#import "MCSwipeTableViewCell+Views.h"
@interface ParticipantTableViewCell : MCSwipeTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end
