//
//  GeneralParticipantTableViewCell.m
//  EventsHub
//
//  Created by alexander on 8/5/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "GeneralParticipantTableViewCell.h"

@implementation GeneralParticipantTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
