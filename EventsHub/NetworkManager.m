//
//  NetworkManager.m
//  EventsHub
//
//  Created by alexander on 7/28/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "NetworkManager.h"
#import "MessageCenter.h"
@implementation NetworkManager
+ (id)sharedNetworkManager
{
    static dispatch_once_t onceQueue;
    static NetworkManager *networkManager = nil;
    
    dispatch_once(&onceQueue, ^{ networkManager = [[self alloc] init]; });
    return networkManager;
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _JSONEncodeHttpRequestManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:HostAPI]];
        
        [_JSONEncodeHttpRequestManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
        
    }
    return self;
}


- (void)perform:(NetworkActionBlock)action{
    
    @autoreleasepool {
        
        action(self,^(void) {
            
            
        },^(NSError *error, NSDictionary *responseObject) {
            
            
            if (responseObject[@"message"]) {
                [MessageCenter showFailed: [NSString stringWithFormat:@"%@ \n %@",responseObject[@"value"], responseObject[@"message"]] inViewController:nil];
            }else{
                [MessageCenter showFailed:error.localizedDescription?error.localizedDescription:responseObject.description inViewController:nil];
            }
            
        });
    }
    
}

@end
