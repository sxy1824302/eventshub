//
//  EventsTableViewController.m
//  EventsHub
//
//  Created by alexander on 6/9/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "EventsTableViewController.h"
#import "ECStep1Controller.h"
#import "EventManager.h"
#import "MyEventInfoCell.h"
#import "AccountControl.h"
#import "EventParticipantsTableViewController.h"
#import "WeixinActivity.h"
#import <MessageUI/MessageUI.h>
#import <RESideMenu.h>
#import "BrowseEventDetialViewController.h"
@interface EventsTableViewController (){
    NSMutableArray*_events;

    NSDateFormatter *_dateFormatter;
}


@end

@implementation EventsTableViewController

- (IBAction)showMenu:(id)sender {
    [self.sideMenuViewController presentLeftMenuViewController];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _dateFormatter =[NSDateFormatter new];
    _dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    _dateFormatter.timeStyle = NSDateFormatterShortStyle;
    

    [[NSNotificationCenter defaultCenter] addObserverForName:AccountLoadedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        
         [self loadEvents];
    }];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self loadEvents];
}
- (void)loadEvents{
    [EventManager  loadEventsCompleted:^(NSArray *events) {
        
        _events = [events mutableCopy];
        
        [self.tableView reloadData];
    }];
    

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_events count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 138;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyEventInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyEventInfoCell" forIndexPath:indexPath];
    
    Event *event = _events[indexPath.row];
    
    cell.event = event;
    

    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
    BrowseEventDetialViewController*bvc = [self.storyboard instantiateViewControllerWithIdentifier:@"BrowseEventDetialViewController"];
    
    Event *event = _events[indexPath.section];
    
    bvc.event = event;
    
    [self.navigationController pushViewController:bvc animated:YES];
    
}


#pragma mark - Navigation

- (IBAction)addEvent:(id)sender {
    
    ECStep1Controller *ecs1 = [ECStep1Controller new];
    
    [self.navigationController pushViewController:ecs1 animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    

}



@end
