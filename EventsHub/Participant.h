//
//  Participant.h
//  EventsHub
//
//  Created by alexander on 7/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <JSONModel.h>

#import "BaseObject.h"

@interface Participant : BaseObject

@property (nonatomic)NSString *name;
@property (nonatomic)NSString *email;
@property (nonatomic)NSString *phone_number;
@property (nonatomic)NSString *attend_date;
@property (nonatomic)NSString *event_ref;
@property (nonatomic)NSString *account_ref;

@property (nonatomic)NSDate *attendDate;


@end
