//
//  NSDate+UTC.h
//  EventsHub
//
//  Created by 孙翔宇 on 8/2/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (UTC)
-(NSString*)UTCString;

+(NSDate*)dateFromUTCString:(NSString*)dS;
@end
