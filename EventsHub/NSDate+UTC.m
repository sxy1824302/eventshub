//
//  NSDate+UTC.m
//  EventsHub
//
//  Created by 孙翔宇 on 8/2/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "NSDate+UTC.h"

static NSDateFormatter *dateFormatter;

@implementation NSDate (UTC)


-(NSString*)UTCString{
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [dateFormatter setTimeZone:timeZone];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
    }
    
    return [dateFormatter stringFromDate:self];
}

+(NSDate*)dateFromUTCString:(NSString*)dS{
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [dateFormatter setTimeZone:timeZone];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
    }
    return [dateFormatter dateFromString:dS];
}
@end
