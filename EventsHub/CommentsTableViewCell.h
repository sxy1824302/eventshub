//
//  CommentsTableViewCell.h
//  EventsHub
//
//  Created by alexander on 8/12/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *commentLabel;

@end
