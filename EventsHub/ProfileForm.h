//
//  ProfileForm.h
//  EventsHub
//
//  Created by alexander on 9/1/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FXForms.h>
@interface ProfileForm : NSObject<FXForm>

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *password;
@property (nonatomic, copy) NSString *repeatPassword;


@property (nonatomic)NSString *name;
@property (nonatomic)NSString *phone;


@end
