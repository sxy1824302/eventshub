//
//  MCSwipeTableViewCell+Views.m
//  EventsHub
//
//  Created by alexander on 8/8/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "MCSwipeTableViewCell+Views.h"

@implementation MCSwipeTableViewCell (Views)
+ (UIView *)viewWithImageName:(NSString *)imageName {
    UIImage *image = [UIImage imageNamed:imageName];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.contentMode = UIViewContentModeCenter;
    return imageView;
}
@end
