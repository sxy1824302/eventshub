//
//  EventBrowseTableViewCell.h
//  EventsHub
//
//  Created by 孙翔宇 on 8/10/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Event;


@protocol EventBrowseDelegate  <NSObject>

-(void)didSelectedCommentOfEvent:(Event*)event;

-(void)didSelectedShareOfEvent:(Event*)event;

@end

@interface EventBrowseTableViewCell : UITableViewCell

@property(nonatomic,weak)id<EventBrowseDelegate>delegate;

@property (nonatomic,weak)Event *event;;

@property (weak, nonatomic) IBOutlet UILabel *eventNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *infomationLabel;

@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *commentsButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@end
