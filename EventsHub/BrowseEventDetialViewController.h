//
//  BrowseEventDetialViewController.h
//  EventsHub
//
//  Created by alexander on 8/18/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "CustomTableViewController.h"
@class Event;
@interface BrowseEventDetialViewController : UIViewController
@property(nonatomic,weak)Event *event;
@end
