//
//  ParticipantsTableViewController.m
//  EventsHub
//
//  Created by alexander on 7/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "ParticipantsTableViewController.h"
#import "EventManager.h"
#import "GeneralParticipantTableViewCell.h"
@interface ParticipantsTableViewController ()
@property (nonatomic)NSArray*participants;

@end

@implementation ParticipantsTableViewController


- (void)loadParticipants{
    
    [EventManager  loadParticipantsCompleted:^(NSArray *participants) {
        
        _participants = participants ;
        
        [self.tableView reloadData];
    }];
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
 
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self loadParticipants];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return _participants.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GeneralParticipantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GeneralParticipantTableViewCell" forIndexPath:indexPath];
    Participant *part =  _participants[indexPath.row];
    
    cell.nameLabel.text = [part name];
    
    cell.contactLabel.text = part.phone_number?part.phone_number:part.email;
    
    return cell;
}

@end
