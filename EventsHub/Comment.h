//
//  Comment.h
//  EventsHub
//
//  Created by alexander on 8/13/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "BaseObject.h"



@interface Comment : BaseObject
@property (nonatomic)NSString *comment;
@property (nonatomic)NSString *account_ref;
@property (nonatomic)NSString *event_ref;

@end
