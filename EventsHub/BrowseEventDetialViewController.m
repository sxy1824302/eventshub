//
//  BrowseEventDetialViewController.m
//  EventsHub
//
//  Created by alexander on 8/18/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "BrowseEventDetialViewController.h"
#import <MGBoxKit.h>
#import <MGButton.h>
#import "Event.h"
#import "EventManager.h"
#import "AccountControl.h"
#import "MessageCenter.h"
#import <MBProgressHUD.h>
#import "EventParticipantsTableViewController.h"
@interface BrowseEventDetialViewController ()<UIScrollViewDelegate>{
    MGScrollView *scroller;
    MGButton *_joinButton;
    MGButton *_likeButton;
    NSArray *_participants;
    
    
    MGLine *_joinCount;
}

@property (nonatomic)BOOL liked;

@end

@implementation BrowseEventDetialViewController

-(void)setLiked:(BOOL)liked{
    _liked = liked;
    if (!liked) {
        _likeButton.tintColor = [UIColor grayColor];
         [_likeButton setTitle:@"Like" forState:UIControlStateNormal];
        
    }else{
         [_likeButton setTitle:@"Dislike" forState:UIControlStateNormal];
         _likeButton.tintColor = [UIColor redColor];
        _likeButton.tintColor = [UIColor redColor];
        
    }
    
    _likeButton.selected =!_likeButton.selected;
    
}
-(void)viewDidLoad{
    [super viewDidLoad];
    

 

    scroller = [MGScrollView scrollerWithSize:self.view.size];
    [self.view addSubview:scroller];
    
    self.navigationItem.title =self.event.name;
    

    


    MGLine *description = [MGLine lineWithSize:(CGSize){320, 80}];
    
    description.padding = UIEdgeInsetsMake(0, 16, 0, 16);
    
    description.leftItems = (id)self.event.event_dsc;
    
    [scroller.boxes addObject:description];
    
    
    

    _joinCount = [MGLine lineWithSize:(CGSize){320, 80}];
    
    _joinCount.padding = UIEdgeInsetsMake(0, 16, 0, 16);
    
    _joinCount.leftItems = (id)[NSString stringWithFormat:@"%i people joined",_participants.count];
    
    [scroller.boxes addObject:_joinCount];
    
    
    [self reloadParticipants];
    
    
    MGLine *phone = [MGLine lineWithSize:(CGSize){320, 40}];
    
    phone.padding = UIEdgeInsetsMake(0, 16, 0, 16);
    
    phone.rightItems =[UIButton buttonWithType:UIButtonTypeContactAdd];
    
    phone.leftItems =(id)[NSString stringWithFormat:@"Phone:%@",self.event.phone_number];
    
    [scroller.boxes addObject:phone];
    
    
    
    MGLine *line1 = [MGLine lineWithSize:(CGSize){320, 100}];
    
    line1.padding = UIEdgeInsetsMake(0, 16, 0, 16);
    

    [scroller.boxes addObject:line1];
    
    if (![_event.created_by isEqualToString:[[[AccountControl sharedAccountControl] account] email]]) {
        
        _joinButton= [[MGButton alloc] initWithFrame:CGRectMake(0, 0, 100, 60)];
        
        [_joinButton addTarget:self action:@selector(join:) forControlEvents:UIControlEventTouchUpInside];
        
        _joinButton.backgroundColor = [UIColor oliveColor];
        
        line1.leftItems = (id)_joinButton;
        
        [_joinButton setTitle:@"Join" forState:UIControlStateNormal];
        
    }
    
    
    _likeButton= [[MGButton alloc] initWithFrame:CGRectMake(0, 0, 100, 60)];
    
    [_likeButton addTarget:self action:@selector(join:) forControlEvents:UIControlEventTouchUpInside];
    
    _likeButton.backgroundColor = [UIColor oliveColor];
    
    [_likeButton addTarget:self action:@selector(like:) forControlEvents:UIControlEventTouchUpInside];
    
    [_likeButton setTitle:@"Like" forState:UIControlStateNormal];
    
    if (line1.leftItems) {
        line1.rightItems = (id)_likeButton;
        
    }else{
        
        line1.leftItems = (id)_likeButton;
        
    }

    
    if ([_event.created_by isEqualToString:[[[AccountControl sharedAccountControl] account] email]]) {
        
        MGButton *joinButton= [[MGButton alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
        
        [joinButton addTarget:self action:@selector(viewParticipants:) forControlEvents:UIControlEventTouchUpInside];
        
        joinButton.backgroundColor = [UIColor oliveColor];
        

        [joinButton setTitle:@"Participants List" forState:UIControlStateNormal];
        
        [scroller.boxes addObject:joinButton];
        
        
    }

    
    
    [scroller layout];
    
    
    
    
    [EventManager canLikeEvent:_event complete:^(NSNumber *liked) {
        
        if (liked.boolValue) {
            
            self.liked = YES;
            
        }else{
            
            self.liked = NO;
        }
        
    }];

}

-(void)viewParticipants:(UIButton*)btn{
    
    EventParticipantsTableViewController*pvc = [self.storyboard instantiateViewControllerWithIdentifier:@"EventParticipantsTableViewController"];
    
    pvc.event = _event;
    
    [self.navigationController pushViewController:pvc animated:YES];
    
}
-(void)like:(UIButton*)btn{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (_likeButton.selected) {
        
        
        [EventManager dislikeEvent:_event complete:^(BOOL success) {
            
            if (success) {
                self.liked = NO;
                
            }
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
    }else{
        
        [EventManager likeEvent:_event complete:^(BOOL success) {
            
            if (success) {
                self.liked = YES;
            }
            
             [MBProgressHUD hideHUDForView:self.view animated:YES];

        }];
        
    }

}
-(void)reloadParticipants{
    [EventManager loadEventParticipants:_event._id completed:^(NSArray *participants) {
        
        _participants = participants;
        
        _joinCount.leftItems = (id)[NSString stringWithFormat:@"%i people joined",participants.count];
        
        [_joinCount layout];
        
        
        [_participants enumerateObjectsUsingBlock:^(Participant* obj, NSUInteger idx, BOOL *stop) {
            
            if ([obj.account_ref isEqualToString: [[[AccountControl sharedAccountControl] account] email]]) {
                
                
                [self selectJoinButton];
                
            }
            
        }];
        
    }];
}
-(void)selectJoinButton{

    _joinButton.selected = YES;
    
    [_joinButton setTitle:@"Quit" forState:UIControlStateNormal];
}

-(void)deseletcJoinButton{
    _joinButton.selected = NO;
    
    [_joinButton setTitle:@"Join" forState:UIControlStateNormal];
}
-(void)join:(UIButton*)join{

    if (!join.selected) {
        if ([[AccountControl sharedAccountControl] canJoinEvent]) {
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            [EventManager joinEvent:_event._id completed:^(BOOL succeed) {
                
                
                if (succeed) {
                    
                    [self reloadParticipants];
                    
                    
                    [self selectJoinButton];
                    
                    
                }
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
            
        }else{
            
            [MessageCenter showFailed:@"You need a valid name and phone number to join an event." inViewController:self];
            
        }
        
    }else{
        
        
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [EventManager quitEvent:_event._id completed:^(BOOL succeed) {
            if (succeed) {
                [self reloadParticipants];
                
                
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self deseletcJoinButton];
                
            }
        }];
        
    }

    

}



@end
