//
//  BrowseEventsTableViewController.m
//  EventsHub
//
//  Created by 孙翔宇 on 8/10/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "BrowseEventsTableViewController.h"
#import "EventBrowseTableViewCell.h"
#import "EventManager.h"
#import "AccountControl.h"
#import "EventHeaderView.h"
#import "CommentsTableViewController.h"
#import "WeixinActivity.h"
#import <RESideMenu.h>
#import "BrowseEventDetialViewController.h"
@interface BrowseEventsTableViewController ()<EventBrowseDelegate>{
    NSArray *_events;
    NSDateFormatter *_dateFormatter;
    UISegmentedControl *_periodFilter;
    
    Event *_selectedEvent;
}

@end

@implementation BrowseEventsTableViewController

- (IBAction)showMenu:(id)sender {
    
    [self.sideMenuViewController presentLeftMenuViewController];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.shyNavBarManager.scrollView = self.tableView;
    
    _dateFormatter =[NSDateFormatter new];
    _dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    _dateFormatter.timeStyle = NSDateFormatterShortStyle;
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    [self.tableView registerClass:[EventHeaderView class]  forHeaderFooterViewReuseIdentifier:@"EventHeaderView"];
    
    
    _periodFilter =[[UISegmentedControl alloc] initWithItems:@[@"Today",@"Tomorrow",@"Weekends"]];
    
    [_periodFilter addTarget:self action:@selector(dateFilterChanged:) forControlEvents:UIControlEventValueChanged];
    
    [_periodFilter setSelectedSegmentIndex:0];
    
    self.navigationItem.titleView = _periodFilter;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:AccountLoadedNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        [self dateFilterChanged:_periodFilter];
    }];
    
}

-(void)dateFilterChanged:(UISegmentedControl*)control{
    PeriodType type;
    switch (control.selectedSegmentIndex) {
        case 0:{
            type = PeriodTypeToday;
            
        break;
        }
        case 1:{
             type = PeriodTypeTomorrow;
            break;
        }
        case 2:{
             type = PeriodTypeWeekends;
            break;
        }
        default:
            break;
    }
    
    [EventManager loadAllEventsWithPeriod:type completed:^(NSArray *events) {
        
        _events = events;
        
        [self.tableView reloadData];
        
    }];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self dateFilterChanged:_periodFilter];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_events count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    EventHeaderView *header =[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"EventHeaderView"];
    
    Event *event = _events[section];
    
    header.dateLabel.text =[_dateFormatter stringFromDate:event.startDate];;
    

    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventBrowseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventBrowseTableViewCell" forIndexPath:indexPath];
    
    Event *event = _events[indexPath.section];
    
    cell.delegate = self;
    
    cell.event = event;
    
    cell.contentView.backgroundColor = [UIColor whiteColor];
    
    return cell;
}




-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 138;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}
-(void)didSelectedShareOfEvent:(Event *)event{
    
    _selectedEvent = event;
    
    
    NSArray* activity = @[[WeixinSessionActivity new], [WeixinTimelineActivity new]];
    
    UIActivityViewController *activityView = [[UIActivityViewController alloc] initWithActivityItems:@[event.name, [UIImage imageNamed:@"icon_session"], [NSURL URLWithString:event.joinURL]] applicationActivities:activity];
    
    activityView.excludedActivityTypes =  @[UIActivityTypeAssignToContact, UIActivityTypeCopyToPasteboard, UIActivityTypePrint,UIActivityTypeAddToReadingList,UIActivityTypeSaveToCameraRoll];
    
    [self presentViewController:activityView animated:YES completion:nil];
}
-(void)didSelectedCommentOfEvent:(Event *)event{
    _selectedEvent = event;
    
    [self performSegueWithIdentifier:@"CommentsSegue" sender:self];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"CommentsSegue"]) {
        CommentsTableViewController *vc = segue.destinationViewController;
        vc.event = _selectedEvent;
    }else{
        NSIndexPath *indexPath =[self.tableView indexPathForSelectedRow];
        
        Event *event = _events[indexPath.section];
        
        BrowseEventDetialViewController *vc = segue.destinationViewController;
        vc.event = event;
        
    }


}
@end
