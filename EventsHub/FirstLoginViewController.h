//
//  FirstLoginViewController.h
//  EventsHub
//
//  Created by alexander on 9/1/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstLoginViewController : UIViewController
-(void)gotoBrowseView;

@end
