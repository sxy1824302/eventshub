//
//  EventHeaderView.h
//  EventsHub
//
//  Created by 孙翔宇 on 8/10/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventHeaderView : UITableViewHeaderFooterView

@property (weak, nonatomic)  UILabel *dateLabel;

@end
