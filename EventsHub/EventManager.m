//
//  EventManager.m
//  EventsHub
//
//  Created by alexander on 7/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "EventManager.h"
#import "NetworkManager.h"
#import <TSMessage.h>
#import "NSDate+UTC.h"
#import "AccountControl.h"
#import "NSDate+UTC.h"
#import "NSDate+Utilities.h"
#import "Comment.h"
@implementation EventManager

+(void)joinEvent:(NSString*)eventId completed:(void(^)(BOOL succeed))completed{
    
    
    Account *account = [[AccountControl sharedAccountControl] account];
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        

        [networkManager.JSONEncodeHttpRequestManager POST:[NSString stringWithFormat:@"events/%@/join",eventId] parameters:@{@"name":account.name,@"phone_number":account.phone,@"email":account.email} success:^(AFHTTPRequestOperation *operation, id responseObject) {


            completed(YES);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            completed(NO);
            failed(error,operation.responseObject);
        }];
        
        
    }];
}
+(void)quitEvent:(NSString*)eventId completed:(void(^)(BOOL succeed))completed{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        
        [networkManager.JSONEncodeHttpRequestManager DELETE:[NSString stringWithFormat:@"events/%@/join",eventId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            completed(YES);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            completed(NO);
            failed(error,operation.responseObject);
        }];
        
        
    }];
}
#pragma mark comments

+(void)commentsCountOfEvent:(Event*)event complete:(void(^)(NSNumber* count))complete{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"events/%@/comment/count",event._id] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            complete(responseObject[@"count"]);
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            complete(@0);
            failed(error,operation.responseObject);
        }];
        
        
    }];
    
}

+(void)getCommentOfEvent:(Event*)event complete:(void(^)(NSArray* comments))completed{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"events/%@/comment",event._id] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSArray*event = [Comment arrayOfModelsFromDictionaries:responseObject];
            
            completed(event);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            completed(nil);
            failed(error,operation.responseObject);
        }];
        
        
    }];
    
}

+(void)commentEvent:(Event*)event withComment:(NSString*)comment complete:(void(^)(BOOL succeed))completed{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager POST:[NSString stringWithFormat:@"events/%@/comment",event._id] parameters:@{
                                                                                                                                  @"comment":comment,
                                                                                                                                  @"lang":[[NSBundle mainBundle] preferredLocalizations][0]}
                                                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            completed(YES);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            completed(NO);
            failed(error,operation.responseObject);
        }];
        
        
    }];
    
}


+(void)likesCountOfEvent:(Event*)event complete:(void(^)(NSNumber* count))complete{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"events/%@/thumbUp/count",event._id] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            complete(responseObject[@"count"]);
           
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
           complete(@0);
            failed(error,operation.responseObject);
        }];
        
        
    }];
    
}
+(void)canLikeEvent:(Event*)event complete:(void(^)(NSNumber* liked))complete{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"events/%@/thumbUp",event._id] parameters:@{@"determine":@YES} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            complete(responseObject[@"liked"]);
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            complete(@0);
            failed(error,operation.responseObject);
        }];
        
        
    }];
    
}


+(void)dislikeEvent:(Event*)event complete:(void(^)(BOOL success))complete{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager DELETE:[NSString stringWithFormat:@"events/%@/thumbUp",event._id] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            complete(YES);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            complete(NO);
            
            failed(error,operation.responseObject);
        }];
        
        
    }];
    
}

+(void)likeEvent:(Event*)event complete:(void(^)(BOOL success))complete{
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager POST:[NSString stringWithFormat:@"events/%@/thumbUp",event._id] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            

            complete(YES);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            complete(NO);
            
            failed(error,operation.responseObject);
        }];
        
        
    }];
    
}

+(void)loadAllEventsWithPeriod:(PeriodType)type completed:(void(^)(NSArray *events))completed{
    
    
    
    NSString*start,*end;
    
    switch (type) {
        case PeriodTypeToday:
            start =[[NSDate date] UTCString];
            end =[[[NSDate date] dateAtEndOfDay] UTCString];
            break;
        case PeriodTypeTomorrow:
            start =[[[NSDate dateTomorrow] dateAtStartOfDay] UTCString];
            end =[[[NSDate dateTomorrow] dateAtEndOfDay] UTCString];
            break;
        case PeriodTypeWeekends:
            start =[[NSDate date] UTCString];
            end =[[[NSDate date] dateAtEndOfDay] UTCString];
            break;
        default:
            break;
    }
    
    
    
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager GET:@"events" parameters:@{@"starts_at":start,@"ends_at":end} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error ;
            NSArray *array = [Event arrayOfModelsFromDictionaries:responseObject error:&error];
            
            
            if (error) {
                
                DDLogVerbose(@"%@",error.localizedDescription);
                
            }else{
                DDLogVerbose(@"have %i events",array.count);
                
            }
            completed(array);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            
        }];
    }];
    
}

+(void)loadEventsCompleted:(void(^)(NSArray *events))completed{
    
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
   
        [networkManager.JSONEncodeHttpRequestManager GET:@"events" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error ;
            NSArray *array = [Event arrayOfModelsFromDictionaries:responseObject error:&error];
            
            
            if (error) {
                DDLogVerbose(@"%@",error.localizedDescription);
                
            }else{
                DDLogVerbose(@"have %i events",array.count);
                
            }
            completed(array);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            
        }];
        
        
    }];
}
+(void)deleteEvent:(Event*)event complete:(void(^)(BOOL success))complete{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
 
        
        [networkManager.JSONEncodeHttpRequestManager DELETE:[NSString stringWithFormat:@"events/%@",event._id] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            complete(YES);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            complete(NO);
            
        }];
        
        
    }];
}

+(void)createEvent:(Event*)event complete:(void(^)(NSDictionary *info))complete{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager POST:@"events" parameters:event.toDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            DDLogVerbose(@"event created");
            
            complete(responseObject);
         
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            complete(nil);
            failed(error,operation.responseObject);
        }];

    }];
}

+(void)updateEvent:(Event*)event complete:(void(^)(BOOL success))complete{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        if (event.address_dsc.length<1) {
            event.address_dsc = @"Location not specified";
        }

        [networkManager.JSONEncodeHttpRequestManager PUT:[NSString stringWithFormat:@"events/%@",event._id] parameters:event.toDictionary success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            
            complete(YES);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            complete(NO);
            
        }];
        
        
    }];
}

+(void)loadEventParticipants:(NSString*)eventsId completed:(void(^)(NSArray *participants))completed{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager GET:[NSString stringWithFormat:@"events/%@/participants",eventsId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error ;
            NSArray *array = [Participant arrayOfModelsFromDictionaries:responseObject error:&error];
            
            
            if (error) {
                DDLogVerbose(@"%@",error.localizedDescription);
                
            }else{
                DDLogVerbose(@"have %i participant",array.count);
                
            }
            completed(array);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            failed(error,operation.responseObject);
        }];
        
        
    }];

}

+(void)participantAttend:(NSString*)participantId completed:(void(^)(void))completed{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        NSString*ds = [[NSDate date] UTCString];
        
        [networkManager.JSONEncodeHttpRequestManager PUT:[NSString stringWithFormat:@"participants/%@",participantId] parameters:@{@"attend_date":ds} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if (responseObject[@"date"]) {
                
            }else{
                failed(nil,operation.responseObject);
            }
            completed();
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
           
            completed();
            
        }];
        
        
    }];
}

+(void)loadParticipantsCompleted:(void(^)(NSArray *participants))completed{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        [networkManager.JSONEncodeHttpRequestManager GET:@"participants" parameters:@{@"account":[[[AccountControl sharedAccountControl] account] email]} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error ;
            NSArray *array = [Participant arrayOfModelsFromDictionaries:responseObject error:&error];
            
            
            if (error) {
                DDLogVerbose(@"%@",error.localizedDescription);
                
            }else{
                DDLogVerbose(@"have %i participant",array.count);
                
            }
            completed(array);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            failed(error,operation.responseObject);
        }];
        
        
    }];
}
@end
