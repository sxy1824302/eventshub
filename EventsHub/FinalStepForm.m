//
//  FinalStepForm.m
//  EventsHub
//
//  Created by alexander on 8/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "FinalStepForm.h"
#import "ThumbnailPickerCell.h"
@implementation FinalStepForm
-(NSArray*)fields{
    return @[
             @{FXFormFieldKey: @"name", FXFormFieldType: FXFormFieldTypeLabel},
             @{FXFormFieldKey: @"basicDescription", FXFormFieldType: FXFormFieldTypeLongText, FXFormFieldPlaceholder: @"Description"},
              @{FXFormFieldKey: @"startDate", FXFormFieldType: FXFormFieldTypeLabel},
             @{FXFormFieldKey: @"thumbnail"},
             @"shareToWechat",
             @"shareToQZone",
             ];
}

@end
