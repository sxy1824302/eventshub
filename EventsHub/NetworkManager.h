//
//  NetworkManager.h
//  EventsHub
//
//  Created by alexander on 7/28/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "NSDate+UTC.h"

@class NetworkManager;
typedef void(^FailedHandler)(NSError *error, NSDictionary *responseObject);

typedef void(^SuccessHandler)(void);
typedef void(^NetworkActionBlock)(NetworkManager *networkManager,SuccessHandler successed, FailedHandler failed);


#define HostAPI @"http://ec2-54-92-15-128.ap-northeast-1.compute.amazonaws.com"

#define HOSTDOMIN @"ec2-54-92-15-128.ap-northeast-1.compute.amazonaws.com"

//#define HostAPI @"http://127.0.0.1:5000"



@interface NetworkManager : NSObject

@property (nonatomic)AFHTTPRequestOperationManager *JSONEncodeHttpRequestManager;

+ (id)sharedNetworkManager;

- (void)perform:(NetworkActionBlock)action;
@end
