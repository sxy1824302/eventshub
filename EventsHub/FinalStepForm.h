//
//  FinalStepForm.h
//  EventsHub
//
//  Created by alexander on 8/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FXForms.h>
@interface FinalStepForm : NSObject<FXForm>
@property (nonatomic)NSString *name;
@property (nonatomic)NSString *basicDescription;
@property (nonatomic)NSDate *startDate;

@property (nonatomic)UIImage *thumbnail;

@property (nonatomic, assign) BOOL shareToWechat;
@property (nonatomic, assign) BOOL shareToQZone;
@end
