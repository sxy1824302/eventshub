//
//  ECStep1Controller.m
//  EventsHub
//
//  Created by alexander on 7/9/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "ECStep1Controller.h"
#import "Event.h"
#import "ECStep2ViewController.h"
#import "Step1Form.h"

@interface ECStep1Controller ()

@end

@implementation ECStep1Controller

- (instancetype)init
{
    self = [super init];
    if (self) {
           self.formController.form = [[Step1Form alloc] init];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(next:)];
    
    self.navigationItem.rightBarButtonItem = item;
    
}
-(void)next:(UIBarButtonItem*)item{
    
    Step1Form *form = (Step1Form*)self.formController.form;
    
    Event *event = [Event new];
    
    event.name = form.name;

    event.event_dsc = form.basicDescription;

    event.phone_number = form.phone;
    
    event.lang = [[NSBundle mainBundle] preferredLocalizations][0];
    
    ECStep2ViewController *ecs1 = [ECStep2ViewController new];
    
    ecs1.even = event;

    [self.navigationController pushViewController:ecs1 animated:YES];
    
}



@end
