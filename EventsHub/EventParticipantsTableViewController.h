//
//  EventDetailTableViewController.h
//  EventsHub
//
//  Created by alexander on 7/11/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTableViewController.h"
@class Event;
@interface EventParticipantsTableViewController : CustomTableViewController
@property (nonatomic,weak)Event *event;
@end
