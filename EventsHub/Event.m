//
//  Event.m
//  EventsHub
//
//  Created by alexander on 7/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "Event.h"
#import "NSDate+UTC.h"
#import "AccountControl.h"
#import "NetworkManager.h"
@implementation Event

@synthesize startDate = _startDate;



+(BOOL)propertyIsIgnored:(NSString *)propertyName{
    
    if ([propertyName isEqualToString:@"participants"]||[propertyName isEqualToString:@"startDate"]||[propertyName isEqualToString:@"likedPeople"]||[propertyName isEqualToString:@"comments"]) {
        return YES;
    }
    return NO;
}


+(BOOL)propertyIsOptional:(NSString *)propertyName{
    if ([propertyName isEqualToString:@"event_dsc"]||[propertyName isEqualToString:@"address_dsc"]||[propertyName isEqualToString:@"longitude"]||[propertyName isEqualToString:@"latitude"]||[super propertyIsOptional:propertyName]||[propertyName isEqualToString:@"lang"]) {
        return YES;
    }
    return NO;
}



- (instancetype)init
{
    self = [super init];
    if (self) {
        _created_by = [[[AccountControl sharedAccountControl] account] email];
    }
    return self;
}



-(NSDate*)startDate{
    return [NSDate dateFromUTCString:_start_date];
}
-(void)setStartDate:(NSDate *)startDate{
    
    _startDate  = startDate;
    
    _start_date = [startDate UTCString];
}

-(NSString*)joinURL{
    return [NSString stringWithFormat:[HostAPI stringByAppendingPathComponent:@"JoinEvent/%@"],self._id];
}


-(NSString*)shareURL{
    return [NSString stringWithFormat:[HOSTDOMIN stringByAppendingPathComponent:@"JoinEvent/%@"],self._id];
}
@end
