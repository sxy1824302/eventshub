//
//  FillProfileViewController.m
//  EventsHub
//
//  Created by alexander on 9/1/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "FillProfileViewController.h"
#import "ProfileForm.h"
#import "AccountControl.h"
#import "FirstLoginViewController.h"
#import "MessageCenter.h"
@interface FillProfileViewController ()

@end

@implementation FillProfileViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.formController.form = [[ProfileForm alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel:)];
    
    UIBarButtonItem *registerBtn = [[UIBarButtonItem alloc] initWithTitle:@"Register" style:UIBarButtonItemStylePlain target:self action:@selector(registerAccount:)];
    
    
    self.navigationItem.leftBarButtonItem = cancel;
    self.navigationItem.rightBarButtonItem = registerBtn;
    
}


-(void)registerAccount:(UIBarButtonItem*)btn{
    [self.tableView resignFirstResponder];
    
    ProfileForm*form = (ProfileForm*)self.formController.form;
    
    NSString *error;
    
    if (!form.email) {
        
        error = @"Email is invalid";
        
    }else if (!form.password){
        
        error = @"password is invalid";
        
    }else if (!form.phone){
        
        error = @"phone is invalid";
        
    }else if (!form.name){
        
        error = @"name is invalid";
    }
    
    if (error) {
        
        [MessageCenter showFailed:error inViewController:self];
        
        return;
    }
    
 
    [[AccountControl sharedAccountControl] createAccountWithEmail:form.email password:form.password phone:form.phone userName:form.name succeed:^(BOOL succeed) {
        
        if (succeed) {
            
            [self.presentingViewController dismissViewControllerAnimated:NO completion:^{
                
                [self.firstController gotoBrowseView];
                
            }];

        }else{
            
        }
    }];
}



-(void)cancel:(UIBarButtonItem*)btn{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}



@end
