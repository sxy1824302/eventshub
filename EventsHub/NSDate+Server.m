//
//  NSDate+Server.m
//  EventsHub
//
//  Created by alexander on 8/13/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "NSDate+Server.h"

static NSDateFormatter *dateFormatter;

@implementation NSDate (Server)
-(NSString*)serverString{
    if (!dateFormatter) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    
    return [dateFormatter stringFromDate:self];
}

+(NSDate*)dateFromServerString:(NSString*)dS{
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    }
    return [dateFormatter dateFromString:dS];
}
@end
