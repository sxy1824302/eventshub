//
//  EventBrowseTableViewCell.m
//  EventsHub
//
//  Created by 孙翔宇 on 8/10/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "EventBrowseTableViewCell.h"
#import "EventManager.h"
#import <TTTLocalizedPluralString.h>

@interface EventBrowseTableViewCell ()
@property (nonatomic)BOOL liked;
@end

@implementation EventBrowseTableViewCell{
    NSNumber *_likesCount;
    NSNumber *_commentsCount;
    
    
}


-(void)setLiked:(BOOL)liked{
    _liked = liked;
    if (!liked) {
        _likeButton.tintColor = [UIColor grayColor];
        
       
    }else{
        
        _likeButton.tintColor = [UIColor redColor];
        
    }
    
     _likeButton.selected =!_likeButton.selected;
    
}
-(void)setEvent:(Event *)event{
    _event = event;
    
    self.eventNameLabel.text = event.name;
    
    self.locationLabel.text = event.address_dsc;
    
     self.infomationLabel.text = [NSString stringWithFormat:@"%@ %@ %@",TTTLocalizedPluralString(_likesCount.intValue, @"like", nil),TTTLocalizedPluralString(_commentsCount.intValue, @"comment", nil),TTTLocalizedPluralString(_event.participants.count, @"join", nil)];
    
    [self updateLikeLabel];
    
    
    [EventManager canLikeEvent:event complete:^(NSNumber *liked) {
        
        if (liked.boolValue) {
            
            self.liked = YES;
            
        }else{
            
            self.liked = NO;
        }
        
    }];
    
}

-(void)updateLikeLabel{
    
    
    [EventManager commentsCountOfEvent:_event complete:^(NSNumber *count) {
        _commentsCount =count;
        
        self.infomationLabel.text = [NSString stringWithFormat:@"%@ %@ %@",TTTLocalizedPluralString(_likesCount.intValue, @"like", nil),TTTLocalizedPluralString(_commentsCount.intValue, @"comment", nil),TTTLocalizedPluralString(_event.participants.count, @"join", nil)];
    }];
    
    
    
    [EventManager likesCountOfEvent:_event complete:^(NSNumber *count) {
        
        _likesCount = count;
        
        
        self.infomationLabel.text = [NSString stringWithFormat:@"%@ %@ %@",TTTLocalizedPluralString(_likesCount.intValue, @"like", nil),TTTLocalizedPluralString(_commentsCount.intValue, @"comment", nil),TTTLocalizedPluralString(_event.participants.count, @"join", nil)];
    }];
    
    
    [EventManager loadEventParticipants:_event._id completed:^(NSArray *participants) {
        
        _event.participants = participants;
        
        self.infomationLabel.text = [NSString stringWithFormat:@"%@ %@ %@",TTTLocalizedPluralString(_likesCount.intValue, @"like", nil),TTTLocalizedPluralString(_commentsCount.intValue, @"comment", nil),TTTLocalizedPluralString(_event.participants.count, @"join", nil)];
    }];
    
    
}
- (IBAction)like:(id)sender {
    
    
    _likeButton.transform = CGAffineTransformMakeScale(0.8, 0.8);
    
    _likeButton.enabled = NO;
    
    [UIView animateWithDuration:0.2 delay:0 usingSpringWithDamping:1 initialSpringVelocity:0 options:0 animations:^{
        
        if (_likeButton.selected) {
            
            
            [EventManager dislikeEvent:_event complete:^(BOOL success) {
                
                if (success) {
                    self.liked = NO;
                    
                }
                [self updateLikeLabel];
                _likeButton.enabled = YES;
            }];
        }else{
            
            [EventManager likeEvent:_event complete:^(BOOL success) {
                
                if (success) {
                    self.liked = YES;
                }
                [self updateLikeLabel];
                _likeButton.enabled = YES;
            }];
           
        }
        
        _likeButton.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        
       
    }];
    
}
- (IBAction)share:(id)sender {
    [self.delegate didSelectedShareOfEvent:_event];
}
- (IBAction)comments:(id)sender {
    

    [self.delegate didSelectedCommentOfEvent:_event];
}

- (void)awakeFromNib
{
    CAShapeLayer *layer = [CAShapeLayer layer];
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(10, 0)];
    [path addLineToPoint:CGPointMake(self.actionView.frame.size.width-20, 0)];
    layer.path = path.CGPath;
    layer.lineWidth=1;
    layer.fillColor = nil;
    layer.strokeColor = [UIColor lightGrayColor].CGColor;
    [self.actionView.layer addSublayer:layer];
    
    
    UIImage *like =  self.likeButton.currentImage ;
    like =[like imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _likeButton.tintColor = [UIColor grayColor];
    [_likeButton setImage:like forState:UIControlStateNormal];
    
    UIImage *comments =  self.commentsButton.currentImage ;
    comments =[comments imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _commentsButton.tintColor = [UIColor grayColor];
    [_commentsButton setImage:comments forState:UIControlStateNormal];
    
    UIImage *share =  self.shareButton.currentImage ;
    share =[share imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _shareButton.tintColor = [UIColor grayColor];
    [_shareButton setImage:share forState:UIControlStateNormal];
    
}



@end
