//
//  MyEventInfoCell.h
//  EventsHub
//
//  Created by alexander on 8/26/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Event;

@protocol MyEventBrowseDelegate  <NSObject>

-(void)didSelectedCommentOfEvent:(Event*)event;

-(void)didSelectedShareOfEvent:(Event*)event;

@end


@interface MyEventInfoCell : UITableViewCell

@property(nonatomic,weak)id<MyEventBrowseDelegate>actionDelegate;

@property (nonatomic,weak)Event *event;;

@property (weak, nonatomic) IBOutlet UILabel *eventNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *infomationLabel;

@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentsButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@end
