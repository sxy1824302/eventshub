//
//  EventManager.h
//  EventsHub
//
//  Created by alexander on 7/25/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Event.h"
#import "Participant.h"


typedef NS_ENUM(NSInteger, PeriodType) {
    PeriodTypeToday,
    PeriodTypeTomorrow,
    PeriodTypeWeekends,
};



@interface EventManager : NSObject

+(void)likesCountOfEvent:(Event*)event complete:(void(^)(NSNumber* count))complete;

+(void)likeEvent:(Event*)event complete:(void(^)(BOOL success))complete;

+(void)dislikeEvent:(Event*)event complete:(void(^)(BOOL success))complete;

+(void)canLikeEvent:(Event*)event complete:(void(^)(NSNumber* liked))complete;




+(void)loadAllEventsWithPeriod:(PeriodType)type completed:(void(^)(NSArray *events))completed;

+(void)loadEventsCompleted:(void(^)(NSArray *events))completed;

+(void)deleteEvent:(Event*)event complete:(void(^)(BOOL success))complete;

+(void)createEvent:(Event*)event complete:(void(^)(NSDictionary *info))complete;

+(void)updateEvent:(Event*)event complete:(void(^)(BOOL success))complete;




+(void)loadEventParticipants:(NSString*)eventsId completed:(void(^)(NSArray *participants))completed;

+(void)participantAttend:(NSString*)participantId completed:(void(^)(void))completed;

+(void)loadParticipantsCompleted:(void(^)(NSArray *participants))completed;



+(void)joinEvent:(NSString*)eventId completed:(void(^)(BOOL succeed))completed;
+(void)quitEvent:(NSString*)eventId completed:(void(^)(BOOL succeed))completed;

+(void)commentEvent:(Event*)event withComment:(NSString*)comment complete:(void(^)(BOOL succeed))completed;
+(void)getCommentOfEvent:(Event*)event complete:(void(^)(NSArray* comments))completed;
+(void)commentsCountOfEvent:(Event*)event complete:(void(^)(NSNumber* count))complete;


@end
