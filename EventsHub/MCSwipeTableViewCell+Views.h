//
//  MCSwipeTableViewCell+Views.h
//  EventsHub
//
//  Created by alexander on 8/8/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "MCSwipeTableViewCell.h"

@interface MCSwipeTableViewCell (Views)
+ (UIView *)viewWithImageName:(NSString *)imageName ;
@end
