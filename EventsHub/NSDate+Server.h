//
//  NSDate+Server.h
//  EventsHub
//
//  Created by alexander on 8/13/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Server)
-(NSString*)serverString;
+(NSDate*)dateFromServerString:(NSString*)dS;
@end
