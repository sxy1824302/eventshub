//
//  LocationManger.h
//  EventsHub
//
//  Created by alexander on 5/30/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^ReverseGeocoding)(NSArray *pois);

@interface LocationManger : NSObject
@property (nonatomic)CLPlacemark *userPlaceMark;
+ (id)sharedLocationManger;
- (void)searchReGeocodeWithLocation:(CLLocationCoordinate2D)location complete:(ReverseGeocoding)complete;
@end
