//
//  ECStep2ViewController.m
//  EventsHub
//
//  Created by alexander on 7/9/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "ECStep2ViewController.h"
#import "EvenLocationPickViewController.h"
#import "Step2Form.h"
#import "Event.h"

@interface ECStep2ViewController ()

@end

@implementation ECStep2ViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.formController.form = [[Step2Form alloc] init];
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(next:)];
    
    self.navigationItem.rightBarButtonItem = item;
}
-(void)next:(UIBarButtonItem*)item{
    
    Step2Form *form = (Step2Form*)self.formController.form;
    
    _even.startDate = form.startDate;
    
    EvenLocationPickViewController*lvc = [self.navigationController.storyboard instantiateViewControllerWithIdentifier:@"EvenLocationPickViewController"];
    
    lvc.event = _even;

    [self.navigationController pushViewController:lvc animated:YES];
}



@end
