//
//  AccountControl.h
//  EventsHub
//
//  Created by alexander on 7/28/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Account.h"

extern NSString *const AccountLoadedNotification ;

@interface AccountControl : NSObject

@property (nonatomic)Account *account;


-(BOOL)canJoinEvent;

+ (id)sharedAccountControl;

-(void)updateAccount:(void(^)(BOOL succeed))completed withInfo:(NSDictionary*)info;


-(void)createAccountWithEmail:(NSString*)email password:(NSString*)password phone:(NSString*)phone userName:(NSString*)name succeed:(void(^)(BOOL succeed))succeed;

-(void)loginWithUserName:(NSString*)username password:(NSString*)password succeed:(void(^)(BOOL succeed))succeed;

@end
