//
//  Account.h
//  EventsHub
//
//  Created by alexander on 8/21/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "JSONModel.h"
#import "BaseObject.h"
@interface Account : JSONModel
@property (nonatomic)NSString *username;
@property (nonatomic)NSString *email;
@property (nonatomic)NSString *name;
@property (nonatomic)NSString *phone;
@property (nonatomic)NSString *session_token;
@end
