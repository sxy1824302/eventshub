//
//  MyEventInfoCell.m
//  EventsHub
//
//  Created by alexander on 8/26/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "MyEventInfoCell.h"
#import "EventManager.h"
#import <TTTLocalizedPluralString.h>
@implementation MyEventInfoCell{
    NSNumber *_likesCount;
    NSNumber *_commentsCount;
    
    
}



-(void)setEvent:(Event *)event{
    _event = event;
    
    self.eventNameLabel.text = event.name;
    
    self.locationLabel.text = event.address_dsc;
    
    self.infomationLabel.text = [NSString stringWithFormat:@"%@ %@ %@",TTTLocalizedPluralString(_likesCount.intValue, @"like", nil),TTTLocalizedPluralString(_commentsCount.intValue, @"comment", nil),TTTLocalizedPluralString(_event.participants.count, @"join", nil)];
    
    [self updateLikeLabel];
    

    
}

-(void)updateLikeLabel{
    
    
    [EventManager commentsCountOfEvent:_event complete:^(NSNumber *count) {
        _commentsCount =count;
        
        self.infomationLabel.text = [NSString stringWithFormat:@"%@ %@ %@",TTTLocalizedPluralString(_likesCount.intValue, @"like", nil),TTTLocalizedPluralString(_commentsCount.intValue, @"comment", nil),TTTLocalizedPluralString(_event.participants.count, @"join", nil)];
    }];
    
    
    
    [EventManager likesCountOfEvent:_event complete:^(NSNumber *count) {
        
        _likesCount = count;
        
        
        self.infomationLabel.text = [NSString stringWithFormat:@"%@ %@ %@",TTTLocalizedPluralString(_likesCount.intValue, @"like", nil),TTTLocalizedPluralString(_commentsCount.intValue, @"comment", nil),TTTLocalizedPluralString(_event.participants.count, @"join", nil)];
    }];
    
    
    [EventManager loadEventParticipants:_event._id completed:^(NSArray *participants) {
        
        _event.participants = participants;
        
        self.infomationLabel.text = [NSString stringWithFormat:@"%@ %@ %@",TTTLocalizedPluralString(_likesCount.intValue, @"like", nil),TTTLocalizedPluralString(_commentsCount.intValue, @"comment", nil),TTTLocalizedPluralString(_event.participants.count, @"join", nil)];
    }];
    
    
}

- (IBAction)share:(id)sender {
    [self.actionDelegate didSelectedShareOfEvent:_event];
}
- (IBAction)comments:(id)sender {
    
    
    [self.actionDelegate didSelectedCommentOfEvent:_event];
}

- (void)awakeFromNib
{
    CAShapeLayer *layer = [CAShapeLayer layer];
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(10, 0)];
    [path addLineToPoint:CGPointMake(self.actionView.frame.size.width-20, 0)];
    layer.path = path.CGPath;
    layer.lineWidth=1;
    layer.fillColor = nil;
    layer.strokeColor = [UIColor lightGrayColor].CGColor;
    [self.actionView.layer addSublayer:layer];
    
    
    UIImage *comments =  self.commentsButton.currentImage ;
    comments =[comments imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _commentsButton.tintColor = [UIColor grayColor];
    [_commentsButton setImage:comments forState:UIControlStateNormal];
    
    UIImage *share =  self.shareButton.currentImage ;
    share =[share imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _shareButton.tintColor = [UIColor grayColor];
    [_shareButton setImage:share forState:UIControlStateNormal];
    
}

@end
