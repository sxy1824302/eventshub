//
//  CommentsTableViewController.m
//  EventsHub
//
//  Created by alexander on 8/12/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "CommentsTableViewController.h"
#import "CommentsTableViewCell.h"
#import "EventManager.h"
#import <PHFComposeBarView.h>
#import "Comment.h"
#import <TTTTimeIntervalFormatter.h>
#import "NSDate+Server.h"
@interface CommentsTableViewController ()<PHFComposeBarViewDelegate>{
    NSArray *_comments;
    TTTTimeIntervalFormatter *_formatter;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *container;

@property (nonatomic) PHFComposeBarView *composeBarView;
@end

@implementation CommentsTableViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _formatter=[TTTTimeIntervalFormatter new];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillToggle:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillToggle:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    
    CGRect frame = CGRectMake(0.0f,
                              self.container.frame.size.height - PHFComposeBarViewInitialHeight,
                              self.container.frame.size.width,
                              PHFComposeBarViewInitialHeight);
    _composeBarView = [[PHFComposeBarView alloc] initWithFrame:frame];
    [_composeBarView setMaxCharCount:160];
    [_composeBarView setMaxLinesCount:5];
    [_composeBarView setPlaceholder:@"Leave a comment..."];
    [_composeBarView setDelegate:self];
    
    [self.container addSubview:_composeBarView];
    
    
    [self reloadComments];
    
}
- (void)keyboardWillToggle:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];
    NSTimeInterval duration;
    UIViewAnimationCurve animationCurve;
    CGRect startFrame;
    CGRect endFrame;
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&duration];
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey]    getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey]        getValue:&startFrame];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey]          getValue:&endFrame];
    
    NSInteger signCorrection = 1;
    if (startFrame.origin.y < 0 || startFrame.origin.x < 0 || endFrame.origin.y < 0 || endFrame.origin.x < 0)
        signCorrection = -1;
    
    CGFloat widthChange  = (endFrame.origin.x - startFrame.origin.x) * signCorrection;
    CGFloat heightChange = (endFrame.origin.y - startFrame.origin.y) * signCorrection;
    
    CGFloat sizeChange = UIInterfaceOrientationIsLandscape([self interfaceOrientation]) ? widthChange : heightChange;
    
    CGRect newContainerFrame = [[self container] frame];
    newContainerFrame.size.height += sizeChange;
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:(animationCurve << 16)|UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [[self container] setFrame:newContainerFrame];
                     }
                     completion:NULL];
}
- (void)composeBarViewDidPressButton:(PHFComposeBarView *)composeBarView {
    
    [EventManager commentEvent:_event withComment:[composeBarView text] complete:^(BOOL succeed) {
        
        [self reloadComments];
    }];
    
    [composeBarView setText:@"" animated:YES];
    [composeBarView resignFirstResponder];
}
-(void)reloadComments{

    [EventManager getCommentOfEvent:_event complete:^(NSArray *comments) {

        _comments = comments;
        
        [self.tableView reloadData];
    }];

   
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self reloadComments];

}
#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return _comments.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentsTableViewCell" forIndexPath:indexPath];
    
    Comment *comment = _comments[indexPath.row];
    
    NSMutableAttributedString*atts = [[NSMutableAttributedString alloc] initWithString:comment.comment attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}];
    
    NSString *interval =   [NSString stringWithFormat:@" %@",[_formatter stringForTimeInterval:[[NSDate dateFromServerString:comment.create_date] timeIntervalSinceNow]]];
    
    [atts appendAttributedString:[[NSAttributedString alloc] initWithString:interval attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12],NSForegroundColorAttributeName:[UIColor lightGrayColor]}]];
    
    cell.commentLabel.attributedText = atts;
    
    return cell;
}

@end
