//
//  BaseObject.m
//  EventsHub
//
//  Created by alexander on 7/28/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "BaseObject.h"

@implementation BaseObject




+(BOOL)propertyIsOptional:(NSString *)propertyName{
    if ([propertyName isEqualToString:@"createdDate"]||[propertyName isEqualToString:@"updatedDate"]) {
        return YES;
    }
    return NO;
}
@end
