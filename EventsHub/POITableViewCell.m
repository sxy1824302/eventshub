//
//  POITableViewCell.m
//  EventsHub
//
//  Created by xiangyu sun on 6/1/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "POITableViewCell.h"

@implementation POITableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
