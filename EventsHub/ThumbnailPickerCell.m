//
//  ThumbnailPickerCell.m
//  EventsHub
//
//  Created by alexander on 9/5/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "ThumbnailPickerCell.h"
#import <ImageIO/ImageIO.h>
@interface ThumbnailPickerCell () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) UIImagePickerController *imagePickerController;

@end

@implementation ThumbnailPickerCell

- (void)setUp
{
    

    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    self.accessoryView = imageView;
    [self setNeedsLayout];
}


- (void)dealloc
{
    _imagePickerController.delegate = nil;
}

- (void)layoutSubviews
{
    CGRect frame = self.imagePickerView.bounds;
    frame.size.height = self.bounds.size.height - 10;
    frame.size.width = self.imagePickerView.image? frame.size.height: 0;
    
    self.imagePickerView.bounds = frame;
    
    [super layoutSubviews];
}

- (void)update
{
    self.textLabel.text = self.field.title;
    self.imagePickerView.image = [self imageValue];
    [self setNeedsLayout];
}

- (UIImage *)imageValue
{
    if (self.field.value)
    {
        return self.field.value;
    }
    else if (self.field.placeholder)
    {
        UIImage *placeholderImage = self.field.placeholder;
        if ([placeholderImage isKindOfClass:[NSString class]])
        {
            placeholderImage = [UIImage imageNamed:self.field.placeholder];
        }
        return placeholderImage;
    }
    return nil;
}

- (UIImagePickerController *)imagePickerController
{
    if (!_imagePickerController)
    {
        _imagePickerController = [[UIImagePickerController alloc] init];
        
        _imagePickerController.delegate = self;
        
        _imagePickerController.allowsEditing = YES;
        
        [self setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    return _imagePickerController;
}

- (UIImageView *)imagePickerView
{
    return (UIImageView *)self.accessoryView;
}

- (BOOL)setSourceType:(UIImagePickerControllerSourceType)sourceType
{
    if ([UIImagePickerController isSourceTypeAvailable:sourceType])
    {
        self.imagePickerController.sourceType = sourceType;
        return YES;
    }
    return NO;
}

- (void)didSelectWithTableView:(UITableView *)tableView controller:(UIViewController *)controller
{
    [self becomeFirstResponder];
    [tableView deselectRowAtIndexPath:tableView.indexPathForSelectedRow animated:YES];
    [controller presentViewController:self.imagePickerController animated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage] ?: info[UIImagePickerControllerOriginalImage];
    
    CGSize imageSize = image.size;
    
    CGFloat width = imageSize.width;
    
    CGFloat height = imageSize.height;
    
    if (width!=height) {
    
        
        CGFloat newDimension = MIN(width, height);
        
        if(width!=height){
            
            CGFloat widthOffset = (width - newDimension) / 2;
            
            CGFloat heightOffset = (height - newDimension) / 2;
            
            UIGraphicsBeginImageContextWithOptions(CGSizeMake(newDimension, newDimension), NO, 0.);
            
            
            
            
            [image drawAtPoint:CGPointMake(-widthOffset, -heightOffset)
                     blendMode:kCGBlendModeCopy
                         alpha:1.];
            
            
            image = UIGraphicsGetImageFromCurrentImageContext();
            
            UIGraphicsEndImageContext();
        }
        
        
        NSData *data = UIImageJPEGRepresentation(image,0.5);
        
        CGImageSourceRef src = CGImageSourceCreateWithData((__bridge CFDataRef)(data), nil);
        
        CFDictionaryRef options = (__bridge_retained  CFDictionaryRef)@{(NSString *)kCGImageSourceCreateThumbnailWithTransform:(id)kCFBooleanTrue,(id)kCGImageSourceCreateThumbnailFromImageAlways:(id)kCFBooleanTrue,(id)kCGImageSourceThumbnailMaxPixelSize:(id)@(80)};
        
        
        CGImageRef thumbnail = CGImageSourceCreateThumbnailAtIndex(src, 0, options);
        
        CFRelease(options);
        
        image = [UIImage imageWithCGImage:thumbnail];
        
        CFRelease(src);
        CGImageRelease(thumbnail);
    }

    self.field.value = image;
    
    
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    self.imagePickerView.image = [self imageValue];
    
    [self setNeedsLayout];
    
    if (self.field.action) self.field.action(self);
}



@end
