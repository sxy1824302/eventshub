//
//  ECFinalViewController.m
//  EventsHub
//
//  Created by alexander on 7/9/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "ECFinalViewController.h"
#import "FinalStepForm.h"
#import "Event.h"
#import "EventManager.h"
#import "WXApi.h"
#import "NetworkManager.h"
#import <MBProgressHUD.h>
#import "ThumbnailPickerCell.h"
@interface ECFinalViewController ()

@end



@implementation ECFinalViewController{
    NSDateFormatter *_dateFormatter;
}

- (instancetype)initWithEvent:(Event*)event
{
    self = [super init];
    if (self) {
        

        _dateFormatter = [NSDateFormatter new];
        
        _dateFormatter.locale = [NSLocale currentLocale];
        
        
        _dateFormatter.dateStyle = NSDateFormatterShortStyle;
        
        _dateFormatter.timeStyle = NSDateFormatterShortStyle;
        
        FinalStepForm *form = [[FinalStepForm alloc] init];;
        self.formController.form =form;
        
        form.name = event.name;
        
        form.basicDescription= event.event_dsc;
        
        form.startDate =event.startDate;
        
        
        
        
        
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(next:)];
        
        self.navigationItem.rightBarButtonItem = item;
        
        self.event = event;
        
        [self.formController registerCellClass:[ThumbnailPickerCell class] forFieldType:FXFormFieldTypeImage];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Post" style:UIBarButtonItemStyleBordered target:self action:@selector(next:)];
    
    self.navigationItem.rightBarButtonItem = item;
}

-(void)next:(UIBarButtonItem*)item{

    FinalStepForm *form = (FinalStepForm*)self.formController.form;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    

    if (form.shareToWechat) {
        
        [self sendWechatContent];
        
    }
    
    if (form.shareToQZone) {

        
    }

    [EventManager updateEvent:_event complete:^(BOOL success){
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (success) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
    }];
    
}


- (void) sendWechatContent
{
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    
    req.scene = WXSceneTimeline;
    
    WXMediaMessage *message = [WXMediaMessage message];
    
    message.title = _event.name;
    
    message.description = [NSString stringWithFormat:@"%@, %@",[_dateFormatter stringFromDate:_event.startDate],_event.address_dsc];
    
    FinalStepForm *form = (FinalStepForm*)self.formController.form;
    
    if (form.thumbnail) {
        
        
        [message setThumbImage:form.thumbnail];
        
    }else{
        
        [message setThumbImage:[UIImage imageNamed:@"icon"]];
        
    }

    WXWebpageObject *ext = [WXWebpageObject object];
    
    ext.webpageUrl = _event.shareURL;

    message.mediaObject = ext;
    
    req.bText = NO;
    
    req.message = message;
    
    [WXApi sendReq:req];
}
@end
