//
//  AccountControl.m
//  EventsHub
//
//  Created by alexander on 7/28/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import "AccountControl.h"
#import "NetworkManager.h"
#import "AccessRecord.h"

#define kLocalRegisterID @"local_device_id"

#define kAccountAccessToken @"stored_account_access_token"

NSString *const AccountLoadedNotification = @"AccountLoadedNotification";

@implementation AccountControl

+ (id)sharedAccountControl
{
    static dispatch_once_t onceQueue;
    static AccountControl *accountControl = nil;
    
    dispatch_once(&onceQueue, ^{ accountControl = [[self alloc] init]; });
    return accountControl;
}
-(BOOL)canJoinEvent{
#warning Do more chaeck
    return self.account.phone.length>1&&self.account.name.length>1;
}
-(void)setAccount:(Account *)account{
    _account = account;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:AccountLoadedNotification object:nil];
    
}
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSString *ludid = [[NSUserDefaults standardUserDefaults] objectForKey:kLocalRegisterID];
        
        if (ludid) {
        
            
        }else{
            
            ludid = [[NSUUID UUID] UUIDString];
            
            [[NSUserDefaults standardUserDefaults] setObject:ludid forKey:kLocalRegisterID];
        }
        
    }
    return self;
}
-(void)createAccountWithEmail:(NSString*)email password:(NSString*)password phone:(NSString*)phone userName:(NSString*)name succeed:(void(^)(BOOL succeed))succeed{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        
        
        [networkManager.JSONEncodeHttpRequestManager POST:@"register" parameters:@{@"email":email,@"password":password,@"phone":phone,@"username":name} success:^(AFHTTPRequestOperation *operation, NSDictionary* responseObject) {
            NSError *error;
            
            Account *account = [[Account alloc] initWithDictionary:responseObject error:&error];
            
            if (error) {
                DDLogError(error.localizedDescription);
            }
            DDLogVerbose(responseObject.description);
            
            self.account = account;
            
            succeed(YES);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            succeed(NO);
            
            failed(error,operation.responseObject);
        }];
        
    }];
    
}
-(void)loginWithUserName:(NSString*)username password:(NSString*)password succeed:(void(^)(BOOL succeed))succeed{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        

        [networkManager.JSONEncodeHttpRequestManager POST:@"login" parameters:@{@"email":username,@"password":password} success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSError *error;
            
            Account *account = [[Account alloc] initWithDictionary:responseObject error:&error];
            
            if (error) {
                DDLogError(error.localizedDescription);
            }

            self.account = account;
            succeed(YES);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            succeed(NO);
            
            failed(error,operation.responseObject);
        }];
        
    }];
    
}
-(void)updateAccount:(void(^)(BOOL succeed))completed withInfo:(NSDictionary*)info{
    
    [[NetworkManager sharedNetworkManager] perform:^(NetworkManager *networkManager, SuccessHandler successed, FailedHandler failed) {
        [networkManager.JSONEncodeHttpRequestManager PUT:[NSString stringWithFormat:@"accounts/%@",_account.email] parameters:info success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            completed(YES);
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            completed(NO);
            failed(error,operation.responseObject);
        }];
    
    }];
    
}


@end
