//
//  MKMapView+Zoom.h
//  EventsHub
//
//  Created by alexander on 5/30/14.
//  Copyright (c) 2014 Uriphium. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (Zoom)
- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated;

-(double) getZoomLevel;
@end
